<img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" align="left" width="144px" height="144px"/>

#### omf_theme_starship
> A theme for [Oh My Fish][omf-link].

[![MIT License][license-badge]](/LICENSE)
[![Fish Shell Version][fish-version]](https://fishshell.com)
[![Oh My Fish Framework][omf-framework]](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>

Remove the default [Fish Shell][fish] prompt to let [Starship][starship] to display its own prompt

## Install

### Fisher

```fish
fisher install gitlab.com/pinage404/omf_theme_starship
```

### Oh My Fish

```fish
omf install https://gitlab.com/pinage404/omf_theme_starship
omf theme omf_theme_starship
# the prompt will disappear until you restart fish
fish # or restart your terminal
```

## Features

* Use [Starship][starship] to display the prompt

## Screenshot

<div align="center">
<img src="https://raw.githubusercontent.com/starship/starship/master/media/demo.gif">
</div>

# License

[MIT][mit] © [pinage404][author] et [al][contributors]

[fish]:           https://fishshell.com/
[omf-link]:       https://www.github.com/oh-my-fish/oh-my-fish
[license-badge]:  https://img.shields.io/badge/license-MIT-007EC7.svg?style=flat-square
[fish-version]:   https://img.shields.io/badge/fish-v3.0.0-007EC7.svg?style=flat-square
[omf-framework]:  https://img.shields.io/badge/Oh%20My%20Fish-Framework-007EC7.svg?style=flat-square
[starship]:       https://starship.rs
[mit]:            https://opensource.org/licenses/MIT
[author]:         https://gitlab.com/pinage404/
[contributors]:   https://gitlab.com/pinage404/omf_theme_starship/-/graphs/master
